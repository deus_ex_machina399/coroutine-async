/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

/** \file This implements a simple TCP echo server using co_routines
 */

#include "async/TcpServer.h"
#include "async/TcpSession.h"
#include "async/Executor.h"
#include "async/Task.h"
#include "async/ReadFuture.h"
#include "async/WriteFuture.h"
#include "async/AcceptFuture.h"

async::Task<int> runSession(std::unique_ptr<async::TcpSession> session)
{
    printf("starting read\n");
    //co_await suspends this coroutine until the read completes
    auto readResult = co_await session->read(255);
    while (readResult.isGood() && !readResult.ok().empty()) {
        printf("result ok: %s\n", readResult.ok().c_str());
        std::string str = readResult.get_ok();
        printf("read: %s\n", str.c_str());
        int strSz = str.size();
        //co_await suspends this coroutine until the write completes
        auto writeResult = co_await session->write(std::move(str));
        printf("Written %zd bytes\n", writeResult.ok());
        if (writeResult.ok() != strSz) {
            break;
        }
        readResult = co_await session->read(255);
    }
    if (!readResult.isGood()) {
        fprintf(stderr, "Error reading from socket %s\n", 
                readResult.error().str());
        co_return -1;
    }
    printf("End of file\n");
    co_return 0;
}

async::Task<int> doAccept(std::unique_ptr<async::TcpServer> server)
{
    while (true) {
        printf("Starting accept\n");
        //co_await suspends this coroutine until the write completes
        auto result = co_await server->accept();
        if (!result.isGood()) {
            fprintf(stderr, "Error accepting connection %s\n",
                    result.error().str());
            co_return -1;
        }
        auto session = result.get_ok();
        printf("Accept complete\n");
        //move the session into a new co_routine that will operate independently
        //of this one
        auto sessionTask = runSession(std::move(session));
        sessionTask.start();
        sessionTask.detach();
    }
    printf("end of doAccept\n");
    co_return 0;
}

int main(int argc, char** argv)
{
    if (argc < 2) {
        fprintf(stderr, "Usage test <port number>\n"); 
        return EXIT_FAILURE;
    }
    
    int port = atoi(argv[1]);
    async::Executor exec;
    auto server = std::make_unique<async::TcpServer>(exec);
    if (server->bind("", port)) {
        abort();
    }
   
    if (server->listen()) {
        abort();
    }
    auto task = doAccept(std::move(server));
    task.start();
    exec.run(); 
    return 0;

}


