#!/bin/bash

echo "Building $0 $1"

if [ "$1" == "debug" ]
then    
    echo Doing debug build
    DEBUG=1
else
    echo Doing release build
    DEBUG=0
fi    

set CXX=clang++
set CC=clang

if [ ! -d build ]
then
    mkdir build
fi
cd build

if [ $DEBUG == 1 ]
then    
    echo Doing debug build
    cmake -DCMAKE_BUILD_TYPE=DEBUG ..
else
    echo Doing release build
    cmake -DCMAKE_BUILD_TYPE=RELEASE ..
fi    

if [ $? -ne "0" ]
then
    echo "cmake failed"
    exit 1
fi    
make -j
if [ $? -ne "0" ]
then
    echo "make failed"
    exit 1
fi    
pushd test
ctest
if [ $? -ne "0" ]
then
    echo "tests failed"
    popd
    exit 1
fi
popd

echo BUILD SUCCESSFUL
