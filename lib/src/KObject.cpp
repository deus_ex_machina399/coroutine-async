/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/KObject.h"

#include "async/Executor.h"
#include <unistd.h>
#include <sys/epoll.h>
#include "Logger.h"

namespace async {

KObject::KObject(Executor& executor)
    : m_executor {executor}
    , m_futures {}
    , m_fd ( -1 )
{ }

KObject::~KObject()
{
    if (m_fd > 0) {
        close(m_fd);
    }
    m_executor.removeKObject(this);
}

ReadFuture KObject::read(size_t bytes)
{
    return ReadFuture(*this, bytes, false);
}

ReadFuture KObject::readAll(size_t bytes)
{
    return ReadFuture(*this, bytes, true);
}

WriteFuture KObject::write(std::string&& data)
{
    return WriteFuture(*this, std::move(data));
}


int KObject::fd() const
{
    return m_fd;
}

void KObject::setFd(int fd)
{
    if(m_fd > 0) {
        m_executor.removeKObject(this);
        ::close(m_fd);
    }
    m_fd = fd;
    if(m_fd > 0) {
        m_executor.addKObject(this);
    }
}
    
void KObject::processEPollResult(uint32_t events, 
        std::function<void(Future* future)> processFuture)
{
    if (events & (EPOLLOUT | EPOLLERR | EPOLLHUP)) {
        processFuture(m_futures[Future::write]);
    }
    if (events & (EPOLLIN | EPOLLERR | EPOLLRDHUP | EPOLLHUP)) {
        processFuture(m_futures[Future::read]);
    }
}
    
void KObject::resetProcessedFuture(Future* future)
{
    m_futures[future->type()] = nullptr;
}
    
Error KObject::addFuture(Future& future)
{
    auto type = future.type();
    if (m_futures[type] ) {
        ASYNC_LOG_ERROR("invalid fd");
        return Error::Err::busy;
    }
    ASYNC_LOG_DEBUG("Adding future %p to executor", &future);
    m_futures[type] = &future;
    return Error::Err::ok;
}

Error KObject::removeFuture(Future& future)
{
    auto type = future.type();
    if (m_futures[type] == &future) {
        ASYNC_LOG_DEBUG("Removing future %p from executor", &future);
        m_futures[type] = nullptr;
        return Error::Err::ok;
    } else {
        return Error::Err::notfound;
    }
}
    
bool KObject::futureActive(Future* future)
{
    return m_futures[future->type()] == future;
}

} //async
