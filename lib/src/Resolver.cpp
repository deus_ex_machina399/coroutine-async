/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Resolver.h"
#include "Logger.h"

namespace async {

Resolver::Resolver()
    : m_hints {}
    , m_port {}
    , m_info { nullptr }
    , m_current { nullptr }
    , m_error { 0 }
{}

Resolver::~Resolver()
{
    if (m_info) {
        freeaddrinfo(m_info);
    }
}

void Resolver::setFlagHint(int flag)
{
    const int acceptedFlags = AI_NUMERICHOST | AI_PASSIVE;
    if (flag != (flag & acceptedFlags)) {
        ASYNC_LOG_ERROR( "Only accepting AI_NUMERICHOST and AI_PASSIVE\n");
    }
    m_hints.ai_flags = flag & acceptedFlags;
}

void Resolver::setAddrFamilyHint(int family)
{
    if (family != AF_INET && family != AF_INET6) {
        ASYNC_LOG_ERROR( "Only accepting AF_INET and AF_INET6\n");
        return;
    }
    m_hints.ai_family = family;
}

void Resolver::setDomain(std::string addr)
{
    m_domain = std::move(addr);
}
    
void Resolver::setPort(uint16_t port)
{
    m_port = port;
}

const std::string& Resolver::domain() const
{
    return m_domain;
}

uint16_t Resolver::port() const
{
    return m_port;
}

bool Resolver::resolve()
{
    //only accepting numeric port numbers and TCP connections for now
    m_hints.ai_flags |= AI_NUMERICSERV;
    m_hints.ai_socktype = SOCK_STREAM;
    char portStr[8] = {};
    snprintf(portStr, sizeof(portStr), "%hd", m_port);
    const char* node = m_domain.empty() ? nullptr : m_domain.c_str();
    int ret = getaddrinfo(
            node, 
            portStr,
            &m_hints,
            &m_info
    );
    if (ret != 0) {
        ASYNC_LOG_ERROR( "Error looking up address %s (%d)\n", gai_strerror(ret), ret);
        m_error = ret;
        return false;
    } else {
        m_current = m_info;
        return true;
    }
}

bool Resolver::next() {
    if (m_current) {
        m_current = m_current->ai_next;
    } else {
        resolve();
    }
    return !!m_current;
}

int Resolver::family() const
{
    return m_current ? m_current->ai_family : AF_UNSPEC;
}

int Resolver::socktype() const 
{
    return m_current ? m_current->ai_socktype : 0;
}

int Resolver::protocol() const
{
    return m_current ? m_current->ai_protocol : 0;
}
    
sockaddr* Resolver::addr() const
{
    return m_current ? m_current->ai_addr : nullptr;
}
    
socklen_t Resolver::addrlen() const
{
    return m_current ? m_current->ai_addrlen : 0;
}

int Resolver::error() const
{
    return m_error;
}

} //async

    

   
