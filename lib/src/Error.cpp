/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include "async/Error.h"
#include <unordered_map>

namespace {
    using ErrorItem = std::pair<async::Error::Err, const char>;
    std::unordered_map<async::Error::Err, const char*> g_errorList = {
#define ERROR_STR(e, c, s) \
            {\
                async::Error::Err::e,\
                s\
            },\
            
            ERROR_TABLE(ERROR_STR)
#undef ERROR_STR                
    };
}

namespace async {

Error::Error(int err)
{ 
    auto it = g_errorList.find(static_cast<Error::Err>(err));
    if (it != g_errorList.end()) {
        m_error = static_cast<Err>(err);
    } else {
        m_error = Err::unknown;
    }
}

Error::Error(Err err)
    : m_error { err }
{}

Error::Err Error::operator()() const
{
    return m_error;
}

const char* Error::str() const
{
    auto it = g_errorList.find(m_error);
    if (it != g_errorList.end()) {
        return it->second;
    } else {
        return "unknown";
    }
}

} //async
