/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/AcceptFuture.h"
#include "async/KObject.h"
#include "async/TcpSession.h"
#include "Logger.h"

#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>


namespace async {

AcceptFuture::AcceptFuture(KObject& kObject)
    : Future(read, kObject)
    {}

AcceptFuture::AcceptFuture(AcceptFuture&& rhs)
    : Future( std::move(rhs) )
    , m_result { std::move(rhs.m_result) }
{}  

int AcceptFuture::sysPoll(KObject& kObject)
{
    int sessionKObject = ::accept4(kObject.fd(), nullptr, nullptr, SOCK_NONBLOCK);
    if (sessionKObject < 0) {
        int ret = errno;
        if (ret != EAGAIN || ret != EWOULDBLOCK) {
            ASYNC_LOG_ERROR("Error accepting socket (%d) %s", errno, strerror(errno));
            m_result = Error(errno);
        }
        return ret;
    }
    m_result = std::make_unique<TcpSession>( kObject, sessionKObject );
    return 0;    
}

AcceptFuture::return_type AcceptFuture::getResult()
{
    return std::move(m_result);
}

AcceptFuture::~AcceptFuture()
{
    printf ("Deleting AcceptFuture %p\n",this);
}
    
void AcceptFuture::setError(Error error)
{
    m_result = error;
}

} //async
