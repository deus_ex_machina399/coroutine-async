/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/Executor.h"
#include "async/Future.h"
#include "async/KObject.h"
#include "Logger.h"

#include <sys/epoll.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>

namespace async {

Executor::Executor()
    : m_epollFd { -1 }
    , m_futureCount{ 0 }
    , m_shuttingDown{ false }
{
    m_epollFd = epoll_create(1);
    if (m_epollFd < 0) {
        perror("Error creating epoll");
    }
}

Executor::~Executor()
{
    m_shuttingDown = true;
    //run();
    ::close(m_epollFd);
}

Error Executor::add(Future& future)
{
    auto kObject = future.m_kObject;
    if (!kObject) {
        ASYNC_LOG_ERROR("invalid fd");
        return Error::Err::invalidKObject;
    }
    if (Error error = kObject->addFuture(future); error) {
        return error;
    }
    m_newFutures.push_back(&future);
    ++m_futureCount;
    ASYNC_LOG_DEBUG("Executor::add m_futureCount = %d", m_futureCount);
    return Error::Err::ok;
}

Error Executor::remove(Future& future)
{
    auto kObject = future.m_kObject;
    if (!kObject) {
        return Error::Err::ok;
    }
    if (Error error = kObject->removeFuture(future); !error) {
        --m_futureCount;
        ASYNC_LOG_DEBUG("Executor::remove m_futureCount = %d", m_futureCount);
        assert(m_futureCount >= 0);
    }
    return Error::Err::ok;
}

Error Executor::addKObject(KObject* kObject)
{
    struct epoll_event event =  {
        .events = EPOLLIN | EPOLLOUT | EPOLLERR | EPOLLHUP | EPOLLET,
        .data = { .ptr = kObject }
    };
    ASYNC_LOG_DEBUG("Adding %d to epoll", kObject->fd());
    Error error = Error::Err::ok; 
    auto ret = epoll_ctl(m_epollFd, EPOLL_CTL_ADD, kObject->fd(), &event);
    if (ret < 0) {
        error = Error(errno);
        ASYNC_LOG_ERROR("Error adding KObject to epoll (%d) %s", errno, error.str());
    }
    return error;
}

Error Executor::removeKObject(KObject* kObject)
{
    struct epoll_event event =  {
        .events = EPOLLIN | EPOLLOUT | EPOLLERR | EPOLLHUP | EPOLLET,
        .data = { .ptr = kObject }
    };
    ASYNC_LOG_DEBUG("Removing %d from epoll", kObject->fd());
    Error error = Error::Err::ok; 
    auto ret = epoll_ctl(m_epollFd, EPOLL_CTL_DEL, kObject->fd(), &event);
    if (ret < 0) {
        error = Error(errno);
        ASYNC_LOG_ERROR("Error removing KObject to epoll (%d) %s", errno, error.str());
    }
    return error;
}

void Executor::processFuture(Future* future)
{
    if (!future) {
        return;
    }
    auto kObject = future->m_kObject;
    if (!kObject) {
        return;
    }
    auto ret = PollResult::error;
    if (m_shuttingDown) {
        future->setError(Error::Err::shuttingdown);
    } else {
        ret = future->poll();
    }
    
    switch (ret) {
    case PollResult::pending:
        return;
    case PollResult::error:
    case PollResult::ok:
        kObject->resetProcessedFuture(future);
        --m_futureCount;
        future->runCallback();
        ASYNC_LOG_DEBUG("Executor::processFuture m_futureCount = %d", m_futureCount);
    }
}

Error Executor::run()
{
    while (m_futureCount > 0) {
        FutureVec futureVec;
        //New futures might be added when executing the old futures. This way we ensure they do not interfere
        //with one another
        std::swap(m_newFutures, futureVec);
        if (m_futureCount > futureVec.size()) {
            //we are waiting for some events via epoll. lets process now
            ssize_t epollRet = 0;
            //We will wait for ever on the first wait but not wait at all on the
            //subsequent waits
            int epollTimeout = m_shuttingDown || futureVec.size() ? 0 : -1;
            const ssize_t kEventBufferSize = 20;
            do {
                std::array<struct epoll_event, kEventBufferSize> events;
                epollRet = epoll_wait(m_epollFd, &events[0], events.size(), epollTimeout);
                if (epollRet < 0) {
                    auto error = Error(errno);
                    ASYNC_LOG_CRITICAL("epoll returned an error (%d) %s", errno, error.str());
                    return error;
                }
                size_t numEvents = static_cast<size_t>(epollRet) > events.size() ? events.size() : epollRet;
                for (size_t i = 0; i < numEvents; ++i) {
                    auto kObject = static_cast<KObject*>(events[i].data.ptr);
                    kObject->processEPollResult(events[i].events, [this](Future* future) {
                        processFuture(future);
                    });    
                }
                epollTimeout = 0;
            } while (epollRet > kEventBufferSize);

        }
        for (auto future : futureVec) {
            auto kObject = future->m_kObject;
            if (kObject && kObject->futureActive(future)) {
                //has not already been processed so safe to process now
                processFuture(future);
            }
        }
    }
    return Error::Err::ok;
}

}
