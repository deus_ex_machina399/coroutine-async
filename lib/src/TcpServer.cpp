/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "Resolver.h"
#include "async/TcpServer.h"
#include "async/Executor.h"
#include "async/AcceptFuture.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

namespace async {

TcpServer::TcpServer(Executor& executor)
    : KObject(executor)
{ }

int TcpServer::bind(const std::string& bindAddress, uint16_t port)
{
    Resolver resolver;
    resolver.setFlagHint(AI_PASSIVE);
    resolver.setDomain(bindAddress);
    resolver.setPort(port);
    if (!resolver.resolve()) {
        return -1;
    }
    int error = 0;
    do {
        int fd = ::socket(resolver.family(), resolver.socktype() | SOCK_NONBLOCK, resolver.protocol());
        if (fd < 0) {
            error = errno;
            fprintf(stderr, "Error opening socket family(%d) type(%d) protocol(%d) - %s\n", 
                    resolver.family(), 
                    resolver.socktype(),
                    resolver.protocol(),
                    strerror(error)
                   );
            error = errno;
            continue;
        }
        int one = 1;
        int ret = ::setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
        if (ret < 0) {
            error = errno;
            fprintf(stderr, "Error setting option socket to %s:%hd : %s\n"
                    , bindAddress.c_str(), port, strerror(error)); 
            close(fd);
            fd = -1;
            continue;
        }
        ret = ::bind(fd, resolver.addr(), resolver.addrlen());
        if (ret < 0) {
            error = errno;
            fprintf(stderr, "Error binding socket to %s:%hd : %s\n"
                    , bindAddress.c_str(), port, strerror(error)); 
            close(fd);
            fd = -1;
            continue;
        }
        //bind successful so we can continue
        setFd(fd);
        printf ("%p binding fd %d\n", this, fd); 
        break;
    } while (resolver.next());
    if (error) {
        fprintf(stderr, "returning error %d\n", error); 
        return error;
    }
    return 0;
}

int TcpServer::listen()
{
    if (::listen(fd(), 3) < 0) {
        int error = errno;
        perror("Error listening");
        return error;
    }
    return 0;
}

AcceptFuture TcpServer::accept() {
    return AcceptFuture(*this);
}

} //async
