/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

namespace async {
    typedef void(*LoggerFn)(void*ctx, int, const char* pattern, ...)
        __attribute__((format(printf,3,4)));
    extern LoggerFn g_logger;
    extern void* g_loggerContext;
}

#define ASYNC_LOG_LEVEL_CRITICAL 0
#define ASYNC_LOG_LEVEL_ERROR 1
#define ASYNC_LOG_LEVEL_WARNING 2
#define ASYNC_LOG_LEVEL_INFO 3
#define ASYNC_LOG_LEVEL_DEBUG 4

#define ASYNC_LOG(logLevel, pattern, ...) \
    g_logger(g_loggerContext, logLevel, __VA_ARGS__)

#if ASYNC_LOG_THRESHOLD >= ASYNC_LOG_LEVEL_CRITICAL
#  define ASYNC_LOG_CRITICAL(...) \
      g_logger(g_loggerContext, ASYNC_LOG_LEVEL_CRITICAL, __VA_ARGS__)
#else
#  define ASYNC_LOG_CRITICAL(...)
#endif
#if ASYNC_LOG_THRESHOLD >= ASYNC_LOG_LEVEL_ERROR
#  define ASYNC_LOG_ERROR(...) \
      g_logger(g_loggerContext, ASYNC_LOG_LEVEL_ERROR, __VA_ARGS__)
#else
#  define ASYNC_LOG_ERROR(...)
#endif
#if ASYNC_LOG_THRESHOLD >= ASYNC_LOG_LEVEL_WARNING
#  define ASYNC_LOG_WARNING(...) \
      g_logger(g_loggerContext, ASYNC_LOG_LEVEL_WARNING, __VA_ARGS__)
#else
#  define ASYNC_LOG_WARNING(pattern, ...)
#endif
#if ASYNC_LOG_THRESHOLD >= ASYNC_LOG_LEVEL_INFO
#  define ASYNC_LOG_INFO(...) \
      g_logger(g_loggerContext, ASYNC_LOG_LEVEL_INFO,  __VA_ARGS__)
#else
#  define ASYNC_LOG_INFO(...)
#endif
#if ASYNC_LOG_THRESHOLD >= ASYNC_LOG_LEVEL_DEBUG
#  define ASYNC_LOG_DEBUG(...) \
      g_logger(g_loggerContext, ASYNC_LOG_LEVEL_DEBUG, __VA_ARGS__)
#else
#  define ASYNC_LOG_DEBUG(...)
#endif

//WE don't want the default leaking out
#undef ASYNC_LOG
