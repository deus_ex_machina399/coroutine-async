/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/Future.h"
#include "async/Executor.h"
#include "async/KObject.h"

#include <errno.h>


namespace async {

Future::Future()
    : m_futureType { empty }
    , m_kObject {nullptr}
{}

Future::Future(Type type, KObject& kObject)
    : m_futureType {type}
    , m_kObject {&kObject}
{}

Future::Future(Future&& rhs)
    : m_futureType { rhs.m_futureType }
    , m_kObject { rhs.m_kObject }
    , m_function(std::move(rhs.m_function))  
{}

Future::~Future()
{
    executor()->remove(*this);
}

PollResult Future::poll()
{
    if (!m_kObject) {
        setError(Error::Err::badf);
        return PollResult::error;
    }
    int error = sysPoll(*m_kObject);
    if (error == EAGAIN || error == EWOULDBLOCK || error == EINPROGRESS) {
        return PollResult::pending;
    } else if (error == 0) {
        return PollResult::ok;
    } else {
        setError(Error(error));
        return PollResult::error;
    }
}

Executor* Future::executor() {
    return m_kObject ? &m_kObject->executor() : nullptr;
}
    
void Future::setCallback(const CallbackType function) {
    m_function = function;
}

void Future::runCallback()
{
    if (m_function) {
        auto func = std::move(m_function);
        setCallback(CallbackType());
        //func must always be the last thing called since
        //this will activate the co_routine
        //which may invalidate the future
        func();
    }
}

} //async
