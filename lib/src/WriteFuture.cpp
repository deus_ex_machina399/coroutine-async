/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/WriteFuture.h"
#include "async/KObject.h"
#include <unistd.h>
#include <errno.h>

namespace async {

WriteFuture::WriteFuture(KObject& kObject, std::string&& data)
    : Future{write, kObject}
    , m_bytesWritten {0}
    , m_data{ std::move(data) }
    , m_error { 0 }
{}

WriteFuture::WriteFuture(WriteFuture&& rhs)
    : Future{ std::move(rhs) }
    , m_bytesWritten { rhs.m_bytesWritten }
    , m_data { std::move( rhs.m_data) }
    , m_error { rhs.m_error }
{}

int WriteFuture::sysPoll(KObject& kObject)
{
    int error = 0;
    do {
        int ret = ::write(kObject.fd(), &m_data[m_bytesWritten], m_data.size() - m_bytesWritten);
        if (ret < 0) {
            error = errno;
        } else {
            m_bytesWritten += ret;
        }
    } while (!error && m_bytesWritten < m_data.size());
    return error;
}

WriteFuture::return_type WriteFuture::getResult()
{
    return m_error() == Error::Err::ok ? return_type(m_bytesWritten) : return_type(m_error);
}

void WriteFuture::setError(Error error)
{
    m_error = error;
}

} //async

