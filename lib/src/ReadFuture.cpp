/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/ReadFuture.h"
#include "async/KObject.h"
#include <unistd.h>
#include <errno.h>

namespace async {

ReadFuture::ReadFuture(KObject& kObject, size_t numBytes, bool readAll)
    : Future{read, kObject }
    , m_numBytes { numBytes }
    , m_error { EAGAIN }
    , m_readAll { readAll }
{}


ReadFuture::ReadFuture(ReadFuture&& rhs)
    : Future{ std::move(rhs) }
    , m_numBytes { rhs.m_numBytes }
    , m_data { std::move(rhs.m_data) }
    , m_error { rhs.m_error }
    , m_readAll { rhs.m_readAll }
{}

int ReadFuture::sysPoll(KObject& kObject)
{
    int error = 0;
    size_t bytesRead = m_data.size();
    m_data.resize(m_numBytes);
    do {
        auto ret = ::read(kObject.fd(), &m_data[bytesRead], m_numBytes - bytesRead);
        if (ret < 0) {
            error = errno;
        } else {
            bytesRead += ret;
        }
    } while (!error && m_readAll && bytesRead < m_numBytes);
    m_data.resize(bytesRead);
    return error;
}

ReadFuture::return_type ReadFuture::getResult()
{
    return m_data.size() > 0 ? return_type(std::move(m_data)) : return_type(m_error);
}

void ReadFuture::setError(Error error)
{
    m_error = error;
}

}
