/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/ConnectFuture.h"
#include "async/KObject.h"
#include "Resolver.h"
#include "Logger.h"

#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>

namespace async {

ConnectFuture::ConnectFuture(KObject& kObject, std::string address, uint16_t port)
    : Future(write, kObject)
    , m_resolver(std::make_unique<Resolver>()) 
    , m_waitingConnect ( false )  
{
    m_resolver->setAddrFamilyHint(AF_INET);
    m_resolver->setDomain(address);
    m_resolver->setPort(port);
}


ConnectFuture::ConnectFuture(ConnectFuture&& rhs)
    : Future(std::move(rhs))
    , m_resolver(std::move(rhs.m_resolver))
    , m_waitingConnect ( false )  
{}


ConnectFuture::~ConnectFuture()
{ }

int ConnectFuture::sysPoll(KObject& kObject)
{
    ASYNC_LOG_DEBUG("ConnectFuture::sysPoll");
    if (!m_waitingConnect) {
        if (!m_resolver->next()) {

            if (m_resolver->error()) {
                m_error = m_resolver->error();
            }   
            return m_error;
        }
        int fd = ::socket(
                m_resolver->family(), 
                m_resolver->socktype()  | SOCK_NONBLOCK, 
                m_resolver->protocol()
                );
        if (fd < 0) {
            ASYNC_LOG_WARNING( "Warning could not open socket family(%d) type(%d) protocol(%d) - %s", 
                    m_resolver->family(), 
                    m_resolver->socktype(),
                    m_resolver->protocol(),
                    strerror(errno)
                   );
            m_error = errno;
            return sysPoll(kObject);
        } else {
            //must add the fd to epoll before connect
            kObject.setFd(fd);
            ASYNC_LOG_DEBUG("Connecting fd = %d", kObject.fd());
            int ret = ::connect(kObject.fd(), m_resolver->addr(), m_resolver->addrlen());
            if (ret < 0) {
                if (errno == EINPROGRESS) {
                    m_waitingConnect = true;
                    m_error = 0;
                    ASYNC_LOG_DEBUG("ConnectFuture::sysPoll - connection in progress");
                    return errno;
                } else {
                    ASYNC_LOG_WARNING("Warning could not connect to %s %hu (%s)"
                            , m_resolver->domain().c_str()
                            , m_resolver->port()
                            , strerror(errno)
                           );
                    m_error = errno;
                    return sysPoll(kObject);
                }
            } else {
                ASYNC_LOG_DEBUG ("Connect completed immediately\n");
                m_error = 0;
                return ret;
            }
        }
    } else {
        ASYNC_LOG_DEBUG("ConnectFuture::sysPoll - checking completion of connection");
        int connectErr = -1;
        socklen_t optSize = sizeof(connectErr);
        int ret = getsockopt( kObject.fd(), SOL_SOCKET, SO_ERROR, &connectErr, &optSize);
        if (ret < 0) {
            ASYNC_LOG_ERROR( "Error getsockopt on fd %d returned  %s\n"
                    , kObject.fd()
                    , strerror(errno)
                   );
            m_error = errno;
            return sysPoll(kObject);
        }
        switch (connectErr) {
        case 0: 
            printf ("Connect completed\n");
            return connectErr;
        case EINPROGRESS:
            return connectErr;
        default:
            ASYNC_LOG_ERROR( "Warning could not complete connect on %s %hu (%s)"
                    , m_resolver->domain().c_str()
                    , m_resolver->port()
                    , strerror(errno)
                   );
            m_error = errno;
            return sysPoll(kObject);
        }
    }
}
    
ConnectFuture::return_type ConnectFuture::getResult()
{
    return m_error == 0 ? return_type(true) : return_type(m_error);
}

void ConnectFuture::setError(Error error)
{
    m_error = static_cast<int>(error());
}

} //async
