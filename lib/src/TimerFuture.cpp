/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/TimerFuture.h"
#include "async/KObject.h"
#include "Logger.h"
#include <string>
#include <unistd.h>

namespace async {

TimerFuture::TimerFuture()
{
    ASYNC_LOG_DEBUG("TimerFuture - Creating Default %p", this);
}
TimerFuture::TimerFuture(KObject& kObject)
    : Future{read, kObject }
{
    ASYNC_LOG_DEBUG("TimerFuture - Creating %p", this);
}

TimerFuture::TimerFuture(TimerFuture&& rhs)
    : Future{ std::move(rhs) }
    , m_result { rhs.m_result }
{
    ASYNC_LOG_DEBUG("TimerFuture - moving %p -> %p", &rhs, this);
}

TimerFuture::~TimerFuture()
{
    ASYNC_LOG_DEBUG("TimerFuture - deleting %p", this);
}

int TimerFuture::sysPoll(KObject& kObject)
{
    int error = 0;
    uint64_t data = 0;
    auto ret = ::read(kObject.fd(), &data, sizeof(data));
    if (ret < 0) {
        error = errno;
    } else {
        m_result = data;
    }
    return error;
}

TimerFuture::return_type TimerFuture::getResult() const
{
    return m_result;
}

void TimerFuture::setError(Error error)
{
    m_result = error;
}

} //async
