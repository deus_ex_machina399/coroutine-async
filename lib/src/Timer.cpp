/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "async/Timer.h"
#include "async/TimerFuture.h"
#include <sys/timerfd.h>
#include <errno.h>
#include <string.h>

namespace async {

Timer::Timer(Executor& executor)
    : KObject(executor)
{
    int fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
    if (fd < 0) {
        fprintf(stderr, "Error creating clock (%d) - %s\n", 
                errno, strerror(errno));
        return;
    }
    setFd(fd);
}

Timer::~Timer()
{}

int Timer::expireAt(TimePoint when)
{
    auto now = std::chrono::system_clock::now();
    auto us = std::chrono::duration_cast<std::chrono::microseconds>(when - now);
    return internalExpireAfter(us);
}
    
TimerFuture Timer::arm() {
    return TimerFuture(*this);
}

int Timer::internalExpireAfter(std::chrono::microseconds us)
{
    auto sec = std::chrono::duration_cast<std::chrono::seconds>(us);
    auto nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(us - sec);
    struct timespec ts = {
        .tv_sec = sec.count(),
        .tv_nsec = nsec.count(),
    };
    struct itimerspec timerspec = {
        .it_interval = { 0 },
        .it_value = ts
    };
    auto ret = timerfd_settime(fd(), 0, &timerspec, nullptr);
    if (ret < 0) {
        fprintf(stderr, "Error setting timer (%d) - %s\n", errno, strerror(errno));
    }
    return ret;
}

}//async
