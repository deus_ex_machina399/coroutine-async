#include "Logger.h"
#include <stdio.h>
#include <stdarg.h>

namespace {
void defaultLogger(void*ctx, int level ,const char* pattern, ...)
{
    auto file = level > ASYNC_LOG_LEVEL_WARNING ? stdout : stderr;
    va_list valist;
    va_start(valist, pattern);
    vfprintf(file, pattern, valist);
    va_end(valist);
    putc('\n', file);
}

}

namespace async {
    LoggerFn g_logger = defaultLogger;
    void* g_loggerContext = nullptr;
}
