/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#pragma once

#define ERROR_TABLE(error) \
    error(ok, 0, "Ok") \
    error(perm, 1, "Operation not permitted") \
    error(noent, 2, "No such file or directory") \
    error(srch, 3, "No such process") \
    error(intr, 4, "Interrupted system call") \
    error(io, 5, "Input/output error") \
    error(nxio, 6, "No such device or address") \
    error(toobig, 7, "Argument list too long") \
    error(noexec, 8, "Exec format error") \
    error(badf, 9, "Bad file descriptor") \
    error(child, 10, "No child processes") \
    error(again, 11, "Resource temporarily unavailable") \
    error(nomem, 12, "Cannot allocate memory") \
    error(acces, 13, "Permission denied") \
    error(fault, 14, "Bad address") \
    error(notblk, 15, "Block device required") \
    error(busy, 16, "Device or resource busy") \
    error(exist, 17, "File exists") \
    error(xdev, 18, "Invalid cross-device link") \
    error(nodev, 19, "No such device") \
    error(notdir, 20, "Not a directory") \
    error(isdir, 21, "Is a directory") \
    error(inval, 22, "Invalid argument") \
    error(nfile, 23, "Too many open files in system") \
    error(mfile, 24, "Too many open files") \
    error(notty, 25, "Inappropriate ioctl for device") \
    error(txtbsy, 26, "Text file busy") \
    error(fbig, 27, "File too large") \
    error(nospc, 28, "No space left on device") \
    error(spipe, 29, "Illegal seek") \
    error(rofs, 30, "Read-only file system") \
    error(mlink, 31, "Too many links") \
    error(pipe, 32, "Broken pipe") \
    error(dom, 33, "Numerical argument out of domain") \
    error(range, 34, "Numerical result out of range") \
    error(deadlk, 35, "Resource deadlock avoided") \
    error(nametoolong, 36, "File name too long") \
    error(nolck, 37, "No locks available") \
    error(nosys, 38, "Function not implemented") \
    error(notempty, 39, "Directory not empty") \
    error(loop, 40, "Too many levels of symbolic links") \
    error(wouldblock, 11, "Resource temporarily unavailable") \
    error(nomsg, 42, "No message of desired type") \
    error(idrm, 43, "Identifier removed") \
    error(chrng, 44, "Channel number out of range") \
    error(l2nsync, 45, "Level 2 not synchronised") \
    error(l3hlt, 46, "Level 3 halted") \
    error(l3rst, 47, "Level 3 reset") \
    error(lnrng, 48, "Link number out of range") \
    error(unatch, 49, "Protocol driver not attached") \
    error(nocsi, 50, "No CSI structure available") \
    error(l2hlt, 51, "Level 2 halted") \
    error(bade, 52, "Invalid exchange") \
    error(badr, 53, "Invalid request descriptor") \
    error(xfull, 54, "Exchange full") \
    error(noano, 55, "No anode") \
    error(badrqc, 56, "Invalid request code") \
    error(badslt, 57, "Invalid slot") \
    error(deadlock, 35, "Resource deadlock avoided") \
    error(bfont, 59, "Bad font file format") \
    error(nostr, 60, "Device not a stream") \
    error(nodata, 61, "No data available") \
    error(time, 62, "Timer expired") \
    error(nosr, 63, "Out of streams resources") \
    error(nonet, 64, "Machine is not on the network") \
    error(nopkg, 65, "Package not installed") \
    error(remote, 66, "Object is remote") \
    error(nolink, 67, "Link has been severed") \
    error(adv, 68, "Advertise error") \
    error(srmnt, 69, "Srmount error") \
    error(comm, 70, "Communication error on send") \
    error(proto, 71, "Protocol error") \
    error(multihop, 72, "Multihop attempted") \
    error(dotdot, 73, "RFS specific error") \
    error(badmsg, 74, "Bad message") \
    error(overflow, 75, "Value too large for defined data type") \
    error(notuniq, 76, "Name not unique on network") \
    error(badfd, 77, "File descriptor in bad state") \
    error(remchg, 78, "Remote address changed") \
    error(libacc, 79, "Can not access a needed shared library") \
    error(libbad, 80, "Accessing a corrupted shared library") \
    error(libscn, 81, ".lib section in a.out corrupted") \
    error(libmax, 82, "Attempting to link in too many shared libraries") \
    error(libexec, 83, "Cannot exec a shared library directly") \
    error(ilseq, 84, "Invalid or incomplete multibyte or wide character") \
    error(restart, 85, "Interrupted system call should be restarted") \
    error(strpipe, 86, "Streams pipe error") \
    error(users, 87, "Too many users") \
    error(notsock, 88, "Socket operation on non-socket") \
    error(destaddrreq, 89, "Destination address required") \
    error(msgsize, 90, "Message too long") \
    error(prototype, 91, "Protocol wrong type for socket") \
    error(noprotoopt, 92, "Protocol not available") \
    error(protonosupport, 93, "Protocol not supported") \
    error(socktnosupport, 94, "Socket type not supported") \
    error(opnotsupp, 95, "Operation not supported") \
    error(pfnosupport, 96, "Protocol family not supported") \
    error(afnosupport, 97, "Address family not supported by protocol") \
    error(addrinuse, 98, "Address already in use") \
    error(addrnotavail, 99, "Cannot assign requested address") \
    error(netdown, 100, "Network is down") \
    error(netunreach, 101, "Network is unreachable") \
    error(netreset, 102, "Network dropped connection on reset") \
    error(connaborted, 103, "Software caused connection abort") \
    error(connreset, 104, "Connection reset by peer") \
    error(nobufs, 105, "No buffer space available") \
    error(isconn, 106, "Transport endpoint is already connected") \
    error(notconn, 107, "Transport endpoint is not connected") \
    error(shutdown, 108, "Cannot send after transport endpoint shutdown") \
    error(toomanyrefs, 109, "Too many references: cannot splice") \
    error(timedout, 110, "Connection timed out") \
    error(connrefused, 111, "Connection refused") \
    error(hostdown, 112, "Host is down") \
    error(hostunreach, 113, "No route to host") \
    error(already, 114, "Operation already in progress") \
    error(inprogress, 115, "Operation now in progress") \
    error(stale, 116, "Stale file handle") \
    error(uclean, 117, "Structure needs cleaning") \
    error(notnam, 118, "Not a XENIX named type file") \
    error(navail, 119, "No XENIX semaphores available") \
    error(isnam, 120, "Is a named type file") \
    error(remoteio, 121, "Remote I/O error") \
    error(dquot, 122, "Disk quota exceeded") \
    error(nomedium, 123, "No medium found") \
    error(mediumtype, 124, "Wrong medium type") \
    error(canceled, 125, "Operation cancelled") \
    error(nokey, 126, "Required key not available") \
    error(keyexpired, 127, "Key has expired") \
    error(keyrevoked, 128, "Key has been revoked") \
    error(keyrejected, 129, "Key was rejected by service") \
    error(ownerdead, 130, "Owner died") \
    error(notrecoverable, 131, "State not recoverable") \
    error(rfkill, 132, "Operation not possible due to RF-kill") \
    error(hwpoison, 133, "Memory page has hardware error") \
    \
    error(shuttingdown, 200, "Executor is shutting down") \
    error(invalidKObject, 201, "KObject is not valid") \
    error(notfound, 202, "item not found") \
    \
    error(unknown, 1000, "Unknown Error Code") \

