/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "async/ReadFuture.h"
#include "async/WriteFuture.h"

#include <array>

namespace async {

class Executor;
class Future;

///This object represents any Kernel Object associated with a file descriptor
///Like all file descriptors, it implements reads and writes
class KObject 
{
public:    
    virtual ~KObject();

    /** Reads from a file descriptor. Future completes successfully when at
     * least 1 bytes is ready 
     *
     * \param bytes - number of bytes to read 
     * \return The Future representing the read 
     */
    ReadFuture read(size_t bytes);

    /** Reads from a file descriptor. Future completes successfully when at
     *  all `bytes` number of bytes have been read 
     *
     * \param bytes - number of bytes to read 
     * \return The Future representing the read 
     */
    ReadFuture readAll(size_t bytes);

    /** Write to a file descriptor. Future completes successfully when at
     *  all `bytes` number of bytes have been written
     *
     * \param bytes - number of bytes to read 
     * \return The Future representing the Write
     */
    WriteFuture write(std::string&& data);

    ///Internal from here down
    int fd() const;
    Executor& executor() {
        return m_executor;
    }

    void setFd(int fd);
    virtual void processEPollResult(uint32_t events, std::function<void(Future* future)> processFuture);
    virtual bool futureActive(Future* future);
    virtual void resetProcessedFuture(Future* future);
    virtual Error addFuture(Future& future);
    virtual Error removeFuture(Future& future);

protected:
    KObject(const KObject&) = delete;
    KObject();
    explicit KObject(Executor& executor);
private:
    Executor& m_executor;
    std::array<Future*, 2> m_futures;
    int m_fd;
};

}
