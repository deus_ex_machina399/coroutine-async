/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "async/Future.h"
#include <coroutine>

namespace async {
    template <typename T>
    class Task;
    
    //private to this file
    namespace detail {
        template <typename T>
        struct Promise;
        template <typename T>
        using HandleType = std::coroutine_handle<Promise<T>>;
        

        template <typename T>
        struct Awaiter {
            explicit Awaiter (HandleType<T> handle)
               : coroutine(handle)
               {} 
            bool await_ready() { return false; }
            HandleType<T> await_suspend(HandleType<T> outer) {
                coroutine.promise().continuation = outer;
                return coroutine;
            }
            T await_resume() {
                return coroutine.promise().value;
            }
            
            HandleType<T> coroutine;
        };
    
    } //detail
    
    /**
     * Object representing a task to be handles by a co-routine
     * e.g. 
     * Task<int> runCoro(int i) {
     *     co_return i + 1;
     * }
     *
     * Template type T is the type co_returned by the Task
     */ 
    template <typename T>
    class Task {
    public:
        using promise_type = detail::Promise<T>;
        Task() 
        {}
        explicit Task(detail::HandleType<T> handle)
           : m_handle (handle) 
        {}
        Task(const Task&) = delete;
        Task(Task&& rhs)
            : m_handle  (std::move(rhs.m_handle))
        {
           rhs.m_handle = detail::HandleType<T>(); 
        }
        ~Task() {
            if (m_handle) {
                m_handle.destroy();
            }
        }
        Task& operator=(const Task&) = delete;
        Task& operator=(Task&& rhs) {
            if (m_handle) {
                m_handle.destroy();
            }
            m_handle = rhs.m_handle;
            rhs.m_handle = detail::HandleType<T>();
        }

        /** Allows chaining of co_awaits between Tasks
         */
        detail::Awaiter<T> operator co_await() noexcept {
            return detail::Awaiter<T> { m_handle };
        }

        /** used to start a suspended coroutine
         */
        void start() {
            m_handle.resume();
        }

        /** 
         * detaches the coroutine from the task. This allows the coroutine to
         * continue to operator when the Task object is deleted in a similar
         * way to detached threads.
         *
         * In a detached state, the coroutine will destroy itself after it completes
         */
        void detach() {
            m_handle.promise().detached = true;
            m_handle = detail::HandleType<T>();
        }

        T get_value() {
            if (m_handle.done()) {
                T val = m_handle.promise().value;
                return val;
            } else {
                return T();
            }
        }

    private:    
        detail::HandleType<T> m_handle;
    };
           
    //Private to this file
    namespace detail {
        template <typename T>
        struct Promise {
            Promise () noexcept 
                : value { T() } 
                , detached { false }
            {}
            ~Promise() {}
            void unhandled_exception() noexcept { std::rethrow_exception( std::current_exception() ); }
            void return_value(T value) noexcept { 
                this->value = value; 
            }
            std::suspend_always initial_suspend() { return {}; }
            struct FinalSuspend {
                bool await_ready() noexcept {
                    return  detached;
                }
                void await_suspend(HandleType<T> handle) {
                    auto cont = handle.promise().continuation;
                    if (cont && !cont.done()) {
                        cont.resume();
                    }
                }

                void await_resume() { }
                bool detached;
            };
            auto final_suspend() {
                return FinalSuspend{ .detached = detached };
            }
            Task<T> get_return_object() { 
                return Task<T> { HandleType<T>::from_promise(*this) }; 
            }
            template <typename F,
                typename std::enable_if<std::is_base_of<Future, F>::value, int>::type = 0
            >
            FutureAwaiter<F> await_transform(F future) {
                return FutureAwaiter<F>(std::move(future));
            }
            
            template <typename F,
                typename std::enable_if<!std::is_base_of<Future, F>::value, int>::type = 0
            >
            F await_transform(F&& item) {
                return std::forward<F>(item);
            }

            T value;
            bool detached;
            HandleType<T> continuation;
        };

    } //detail
} //async

