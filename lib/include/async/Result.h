/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#pragma once

#include "Error.h"
#include <type_traits>
#include <utility>

namespace async {

enum class PollResult { pending, ok, error }; 

template <typename T>
class Result
{
    static_assert(!std::is_same<T, Error>::value, "Cannot use an error type for result");
public:    

    Result() 
        : m_state { PollResult::pending }
        , m_ok { }
        , m_error { Error::Err::again }
    {}
    Result(T ok) 
        : m_state { PollResult::ok }
        , m_ok { ok }
        , m_error { Error::Err::ok }
    {}
    Result(Error err) 
        : m_state { PollResult::error }
        , m_ok {}
        , m_error { err }
    {}
    
    const Result& operator=(T&& ok)
    {
        m_state = PollResult::ok;
        m_ok = std::forward<T>(ok);
        m_error = Error::Err::ok;
        return *this;
    }
    const Result& operator=(Error err)
    {
        m_state = PollResult::error;
        m_ok = T{};
        m_error = err;
        return *this;
    }
    
    PollResult state () const {
        return m_state;
    }

    bool isGood() const {
        return m_state == PollResult::ok;
    }

    const T& ok() const {
        return m_ok;
    }
    T get_ok() {
        return std::move(m_ok);
    }

    const Error error() const {
        return m_error;
    }

private:
    PollResult m_state;
    T m_ok;
    Error m_error;
};

} //async
