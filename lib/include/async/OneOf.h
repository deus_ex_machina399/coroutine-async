/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


#include <tuple>
#include <coroutine>

namespace async {

template <typename ...Future>    
class OneOf {
public:
    using FutureList = std::tuple<Future...>;
    using FutureListSize = std::tuple_size<FutureList>;
    using ReturnType = ssize_t;

    explicit OneOf(Future... futures);
    OneOf(OneOf&& rhs);
    bool await_ready() const noexcept;
    void await_suspend(std::coroutine_handle<> handle);
    ReturnType await_resume();

private:
    template <int I = 0>
    void setupFuture(std::coroutine_handle<> handle);

    FutureList m_futures;
    ReturnType m_ret;

};

template <typename ...Future>    
OneOf<Future...>::OneOf(Future... futures)
    : m_futures { (std::move(futures))... }
    , m_ret { -1 }
{} 
    
template <typename ...Future>
OneOf<Future...>::OneOf(OneOf&& rhs)
    : m_futures { std::move(rhs.m_futures) }
    , m_ret { rhs.m_ret }
{}
    
template <typename ...Future>
template <int I>
void OneOf<Future...>::setupFuture(std::coroutine_handle<> handle)
{
    using CurrentType = typename std::tuple_element<I, OneOf::FutureList>::type;
    CurrentType* future = &std::get<I>(m_futures);
    future->setCallback([this, handle] () mutable {
        CurrentType future(std::move(std::get<I>(m_futures)));
        m_ret = I;
        handle.resume();
    });
    future->executor()->add(*future);
    if constexpr ( I + 1 < FutureListSize::value) {
        setupFuture<I+1>(handle);
    }
}

template <typename ...Future>    
bool OneOf<Future...>::await_ready() const noexcept
{
    return false;
}

template <typename ...Future>    
void OneOf<Future...>::await_suspend(std::coroutine_handle<> handle)
{
    setupFuture<>(handle);
}
template <typename ...Future>    
auto OneOf<Future...>::await_resume() -> ReturnType
{
    return m_ret;
}

} //async
            

