/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "async/Error.h"
#include <vector>

namespace async {

class KObject;
class Future;

/**
 * This class acts as the executor for your program
 * Somewhere early in the running of your program you will add some Futures for the
 * Executor to process and then call the  `run` method. The run method then acts
 * as your programs main run which will process the Futures when they become
 * ready
 *
 * `run` depends on `epoll` which is only implemented on Linux so will not work
 * on other operating systems.
 *
 * `run returns when there is no longer any work to do.
 */
class Executor
{
public:
    ///Constructor
    Executor();
    ///Destructor
    ~Executor();

    ///Adds a future to the executor for processing when it becomes ready
    Error add(Future& future);
    
    ///remove a future from the executor
    Error remove(Future& future);

    /// The main application event loop
    Error run();

    // From here
    Error addKObject(KObject* kObject);
    Error removeKObject(KObject* kObject);
private:
    void processFuture(Future* future);

    int m_epollFd;
    using FutureVec = std::vector<Future*>;
    FutureVec m_newFutures;
    size_t m_futureCount;
    bool m_shuttingDown;
};

} //async
