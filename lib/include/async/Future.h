/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#pragma once

#include "Result.h"
#include <functional>
#include <coroutine>
#include <errno.h>

namespace async {

class Executor;
class KObject;

/**
 * This is the non-templated base class of all Futures.
 * A is returned for any asynchronous operation returned by this library
 *
 * Given that the operation is asynchronous, the result will only become
 * available at some later point in time. The allows the user to query that
 * future state.
 *
 * The user can test the state of the Future at any later point in time by
 * calling the `poll` function. The `poll` function will try to complete the
 * asynchronous operation and returns a `PollResult` representing the state of
 * the future.
 * 
 * In practice, constantly polling Futures is inefficient so we get the Executor
 * to do this for us. The Executor will use the internal file descriptor to help
 * know when the Future may be ready.
 *
 * Calling `Future::poll()` on a destroyed KObject is undefined behaviour 
 */ 
class Future
{
    friend class Executor;
public:
    /**
     * polls the future. This tries to complete the future and return the
     * result
     */ 
    PollResult poll();

    /**
     * returns the error code or zero if no error occurred
     */
    int getError() const;
    
    ///Destructor
    virtual ~Future();

    //internal to the library from here down
    
    using CallbackType = std::function<void()>;
    enum Type { empty = -1, read = 0, write = 1 };
    
    Executor* executor();
    Type type() const {
        return m_futureType;
    }
    void setCallback(const CallbackType function);
    
    Future(const Future&) = delete;
    Future& operator=(const Future&) = delete;
protected:
    Future();
    explicit Future(Type type, KObject& fd);
    Future(Future&& rhs);

    ///implemented by derived classes to perform the necessary calls to
    ///actually poll the file descriptor
    virtual int sysPoll(KObject& fd) = 0;
    virtual void setError(Error error) = 0;
    void runCallback();

    Type m_futureType;
    KObject* m_kObject;
    CallbackType m_function;
};


/** Instantiated whenever we co_await a Future
 */
template <typename F>
class FutureAwaiter
{
public:    
    explicit FutureAwaiter(F&& future);
    bool await_ready() const noexcept;
    void await_suspend(std::coroutine_handle<> handle);
    auto await_resume(); 

private:
     FutureAwaiter() = default;
     F m_future;
};


template <typename F>
FutureAwaiter<F>::FutureAwaiter(F&& future)
    : m_future(std::move(future))
{}


template <typename F>
void FutureAwaiter<F>::await_suspend(std::coroutine_handle<> handle) 
{
    auto resume_func = [handle] () mutable {
        //need this to restore this
        handle.resume();
    };
    m_future.setCallback(resume_func);
    auto exec = m_future.executor();
    if (exec) {
        exec->add(m_future);
    } else {
        handle.resume();
    }
}

template <typename F>
auto FutureAwaiter<F>::await_resume()
{
    return m_future.getResult();
}

template <typename F>
bool FutureAwaiter<F>::await_ready() const noexcept 
{
    return false;
}

} //async
