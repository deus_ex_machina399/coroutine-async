#include <sys/socket.h>
#include <sys/types.h>
#include <exception>
#include <string>
#include <array>


class SockEx : public std::exception
{
public:    
    explicit SockEx(const char *, ...) noexcept 
        __attribute__((format (printf, 2,3)));
    const char* what() const noexcept override;
private:
    std::array<char, 255> m_buf;   
};

class TestSocket
{
public:    
    TestSocket();
    ~TestSocket();
    TestSocket(const TestSocket&) = delete;
    TestSocket(TestSocket&& rhs);
    TestSocket& operator=(const TestSocket&) = delete;
    TestSocket& operator=(TestSocket&& rhs);
    void bind(const std::string& sAddr, uint16_t port);
    void create();
    void listen();
    TestSocket accept();
    void connect(std::string& address, uint16_t port);
    std::string read(size_t len);
    void write(const std::string& data);
private:
    explicit TestSocket(int fd);
    int m_fd;
};
