/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "gtest/gtest.h"
#include "testsocket.h"
#include "async/Executor.h"
#include "async/TcpConnector.h"
#include "async/ConnectFuture.h"
#include "async/TcpServer.h"
#include "async/TcpSession.h"
#include "async/AcceptFuture.h"
#include "async/Task.h"
#include <future>

TEST(Basic_Socket, PassThroughExecutor) {
    async::Executor exec;
    exec.run();
}

    
const std::string kmarco = "marco";
const std::string kpolo = "polo";
const uint16_t kPort = 20000;

bool acceptMarcoPoloThread(TestSocket testSocket)
{
    try {
        printf("acceptMarcoPolo waiting for accept\n");
        auto session = testSocket.accept();
        printf("acceptMarcoPolo new connection accepted\n");
        auto red = session.read(kmarco.size());
        if (red != kmarco) {
            fprintf(stderr, "Read %s, expected %s\n", red.c_str(), kmarco.c_str());
            return false;
        }
        printf("acceptMarcoPolo read completed\n");
        session.write(kpolo);
        printf("acceptMarcoPolo write completed\n");
        return true;
    } catch (const std::exception& e) {
        fprintf(stderr, "acceptMaroPolo exception %s\n", e.what());
        return false;
    }
} 

async::Task<bool> connectMarcoPoloCoro(std::unique_ptr<async::TcpConnector> conn)
{
    printf("connectMarcoPoloCoro waiting for connect\n");
    
    if (auto result = co_await conn->connect("localhost", kPort); !result.isGood()) {
        co_return false;
    }
    printf("connectMarcoPoloCoro connect completed \n");
    if (auto result = co_await conn->write(std::string(kmarco)); !result.isGood()) {
        co_return false;
    }
    printf("connectMarcoPoloCoro write completed \n");
    auto readResult = co_await conn->read(kpolo.size());
    auto red = readResult.ok();
    if ( red != kpolo) {
        printf("got %s - expected %s\n", red.c_str(), kpolo.c_str());
        co_return false;
    }
    printf("connectMarcoPoloCoro read completed \n");
    co_return true;
}

async::Task<bool> acceptMarcoPoloCoro(std::unique_ptr<async::TcpServer> server)
{
    printf("acceptMarcoPoloCoro waiting for accept\n");
    auto acceptResult = co_await server->accept();
    if (!acceptResult.isGood()) {
        co_return false;
    }
    auto session = acceptResult.get_ok();
    printf("acceptMarcoPoloCoro new connection accepted\n");
    auto readResult = co_await session->read(kmarco.size());
    auto red = readResult.ok();
    if (red != kmarco) {
        fprintf(stderr, "Read %s, expected %s\n", red.c_str(), kmarco.c_str());
        co_return false;
    }
    printf("acceptMarcoPoloCoro read completed\n");
    auto writeResult = co_await session->write(std::string(kpolo));
    if (writeResult.ok() != kpolo.size()) {
        co_return false;
    }
    printf("acceptMarcoPoloCoro write completed\n");
    co_return true;
} 

bool connectMarcoPoloThread(TestSocket testSocket)
{
    try {
        printf("connectMarcoPoloThread waiting for connect\n");
        auto addr = std::string("127.0.0.1");
        testSocket.connect(addr, kPort);
        printf("connectMarcoPoloThread connect completed \n");
        testSocket.write(std::string(kmarco));
        printf("connectMarcoPoloThread write completed \n");
        auto red = testSocket.read(kpolo.size());
        if ( red != kpolo) {
            printf("got %s - expected %s\n", red.c_str(), kpolo.c_str());
            return false;
        }
        printf("connectMarcoPoloThread read completed \n");
        return true;
    } catch (const std::exception& e) {
        fprintf(stderr, "connectMarcoPoloThread exception %s\n", e.what());
        return false;
    }
}


TEST(Basic_Socket, Connect) {
    TestSocket testSocket;
    testSocket.create();
    testSocket.bind("", kPort);
    testSocket.listen();

    auto remote = std::async(std::launch::async, 
            acceptMarcoPoloThread, std::move(testSocket));

    async::Executor exec;
    auto conn = std::make_unique<async::TcpConnector>(exec);
    auto task = connectMarcoPoloCoro(std::move(conn));
    task.start();

    exec.run();
    ASSERT_EQ(remote.get(), true);
    ASSERT_EQ(task.get_value(), true);
}

TEST(Basic_Socket, Accept) {
    async::Executor exec;

    auto server = std::make_unique<async::TcpServer>(exec);
    ASSERT_EQ(server->bind("", kPort), 0);
    ASSERT_EQ(server->listen(), 0);

    TestSocket testSocket;
    testSocket.create();

    auto task = acceptMarcoPoloCoro(std::move(server));
    task.start();

    auto remote = std::async(std::launch::async, 
            connectMarcoPoloThread, std::move(testSocket));
    exec.run();
    ASSERT_EQ(remote.get(), true);
    ASSERT_EQ(task.get_value(), true);
}

TEST(Basic_Socket, AcceptConnect) {
    async::Executor exec;

    auto server = std::make_unique<async::TcpServer>(exec);
    ASSERT_EQ(server->bind("", kPort), 0);
    ASSERT_EQ(server->listen(), 0);

    auto conn = std::make_unique<async::TcpConnector>(exec);
    auto connectTask = connectMarcoPoloCoro(std::move(conn));
    connectTask.start();

    auto acceptTask = acceptMarcoPoloCoro(std::move(server));
    acceptTask.start();

    exec.run();
    ASSERT_EQ(connectTask.get_value(), true);
    ASSERT_EQ(acceptTask.get_value(), true);
}
