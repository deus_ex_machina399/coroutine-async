/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gtest/gtest.h"
#include "async/EventFd.h"
#include "async/Timer.h"
#include "async/TimerFuture.h"
#include "async/Task.h"
#include "async/EventFuture.h"
#include "async/Executor.h"
#include <async/OneOf.h>
#include <chrono>
#include <variant>
#include <thread>

using namespace std::chrono_literals;

async::Task<bool> noYieldCoro(async::Executor& exec)
{
    auto duration = 500ms;
    auto tol = 5ms;
    
    async::Timer timer(exec);
    async::EventFd eventFd(exec);
    printf("yielding to event\n");
    auto start = std::chrono::system_clock::now();
    timer.expireAfter(duration);
    async::OneOf either(eventFd.wait(), timer.arm());
    auto ret = co_await either;
    auto end = std::chrono::system_clock::now();
    if (ret != 1) {
        printf("event yielded instead of timer\n");
        co_return false;
    }
    if (end - start < duration - tol) {
        printf("expired too late\n");
        co_return false;
    }
    co_return true;
}

TEST(Event, NoSignal) {
    async::Executor exec;
    auto task = noYieldCoro(exec);
    task.start();
    exec.run();
    ASSERT_EQ(task.get_value(), true);
}

async::Task<bool> yieldCoro(async::Executor& exec)
{
    auto duration = 500ms;
    auto tol = 5ms;
    
    async::Timer timer(exec);
    async::EventFd eventFd(exec);
    printf("yielding to event\n");
    auto start = std::chrono::system_clock::now();
    timer.expireAfter(duration);
    async::OneOf either(eventFd.wait(), timer.arm());
    eventFd.signal();
    auto ret = co_await either;
    auto end = std::chrono::system_clock::now();
    if (ret != 0) {
        printf("timer yielded instead of event\n");
        co_return false;
    }
    if (end - start > duration - tol) {
        printf("expired too early\n");
        co_return false;
    }
    co_return true;
}

TEST(Event, Signal) {
    async::Executor exec;
    auto task = yieldCoro(exec);
    task.start();
    exec.run();
    ASSERT_EQ(task.get_value(), true);
}

async::Task<bool> multiThreadCoro(async::Executor& exec)
{
    const auto duration = 500ms;
    const auto tol = 5ms;
    
    async::EventFd eventFd(exec);
    printf("yielding to event\n");
    async::EventFd::Signaller signaller = eventFd.getSignaller();
    auto thread = std::thread([signaller, duration]() {
        std::this_thread::sleep_for(duration);
        signaller.signal();
    });
    auto start = std::chrono::system_clock::now();
    auto ret = co_await eventFd.wait();
    auto end = std::chrono::system_clock::now();
    thread.detach();
    if (!ret.isGood() || !ret.ok()) {
        printf("timer yielded instead of event\n");
        co_return false;
    }
    if (end - start < duration - tol) {
        printf("expired too early\n");
        co_return false;
    }
    co_return true;
}

TEST(Event, MultiThread) {
    async::Executor exec;
    auto task = multiThreadCoro(exec);
    task.start();
    exec.run();
    ASSERT_EQ(task.get_value(), true);
}
