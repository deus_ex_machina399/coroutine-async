/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "gtest/gtest.h"
#include "async/Timer.h"
#include "async/Task.h"
#include "async/TimerFuture.h"
#include "async/Executor.h"
#include <async/OneOf.h>
#include <chrono>
#include <variant>

using namespace std::chrono_literals;

async::Task<bool> expireAfterCoro(async::Executor& exec)
{
    async::Timer timer(exec);
    printf("ExpireAfterCoro arming timer\n");
    auto start = std::chrono::system_clock::now();
    auto duration = 500ms;

    timer.expireAfter(duration);
    co_await timer.arm();
    auto end = std::chrono::system_clock::now();

    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>
        (end - start);
    printf( "%llu milliseconds have elapsed\n", diff.count());

    if (diff < duration) {
        co_return false;
    }

    auto toll = 2s;
    if (diff > duration + toll) {
        co_return false;
    }
    co_return true;
}

TEST(Timer, ExpireAfter) {
    async::Executor exec;
    auto task = expireAfterCoro(exec);
    task.start();
    exec.run();
    ASSERT_EQ(task.get_value(), true);
}

async::Task<bool> expireAtCoro(async::Executor& exec)
{
    async::Timer timer(exec);
    printf("ExpireAfterCoro arming timer\n");
    auto duration = 500ms;

    auto start = std::chrono::system_clock::now();
    timer.expireAt(start + duration);
    co_await timer.arm();
    auto end = std::chrono::system_clock::now();

    auto diff = end - start;
    printf( "%llu milliseconds have elapsed\n", diff.count());

    if (diff < duration) {
        co_return false;
    }

    auto toll = 500ms;
    if (diff > duration + toll) {
        co_return false;
    }
    co_return true;
}

TEST(Timer, ExpireAt) {
    async::Executor exec;
    auto task = expireAtCoro(exec);
    task.start();
    exec.run();
    ASSERT_EQ(task.get_value(), true);
}

async::Task<bool> eitherCoro(async::Executor& exec) 
{
    async::Timer timer1(exec);
    async::Timer timer2(exec);
    auto duration1 = 500ms;
    auto duration2 = 2000ms;

    auto start = std::chrono::system_clock::now();
    timer1.expireAfter(duration1);
    timer2.expireAfter(duration2);
    printf("Adding timers to OneOf\n");
    async::OneOf either(timer2.arm(), timer1.arm());
    auto ret = std::move(co_await either);
    printf("Timer %zd expired\n", ret);
    if (ret != 1) {
        co_return false;
    }
    auto end = std::chrono::system_clock::now();

    auto diff = end - start;
    printf( "%llu milliseconds have elapsed\n", 
        std::chrono::duration_cast<std::chrono::milliseconds>(diff).count());

    if (diff < duration1) {
        co_return false;
    }

    auto toll = 500ms;
    if (diff > duration1 + toll) {
        co_return false;
    }
    co_return true;

}

TEST(Timer, Either) {
    async::Executor exec;
    auto task = eitherCoro(exec);
    task.start();
    exec.run();
    printf("run completed\n");
    ASSERT_EQ(task.get_value(), true);
}
