/*
Copyright 2019 Doron Wloschowsky

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors
may be used to endorse or promote products derived from this software without
specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "testsocket.h"
#include <stdarg.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
    
SockEx::SockEx(const char *fmt, ...) noexcept
{
    va_list va;
    va_start(va, fmt);
    vsnprintf(m_buf.data(), m_buf.size(), fmt, va);
    va_end(va);
}

const char* SockEx::what() const noexcept
{
    return m_buf.data();
}

TestSocket::TestSocket()
    : m_fd ( -1 )
{}


TestSocket::TestSocket(int fd)
    : m_fd ( fd )
{}

TestSocket::TestSocket(TestSocket&& rhs)
    : m_fd ( rhs.m_fd )
{
    rhs.m_fd = -1;
}
    
TestSocket& TestSocket::operator=(TestSocket&& rhs)
{
    m_fd = rhs.m_fd;
    rhs.m_fd = -1;
    return *this;
}


TestSocket::~TestSocket()
{
    if (m_fd > 0) {
        ::close(m_fd);
    }
}

void TestSocket::create()
{
    auto fd = ::socket(AF_INET, SOCK_STREAM, 0);
    if (fd < 0) {
        throw SockEx("Error opening socket %d, %s\n", errno, strerror(errno));
    }
    m_fd = fd;
}

struct sockaddr_in addrFromString(std::string str, uint16_t port)
{
    struct sockaddr_in ret = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
    };
    if (str.size() == 0) {
        ret.sin_addr.s_addr = INADDR_ANY;
        ret.sin_family = AF_INET;
        return ret;
    } else {
        switch (inet_pton(AF_INET, str.c_str(), &ret.sin_addr)) {
        case 1:
            return ret;
        case 0:
            throw SockEx("%s is an invalid ip address", str.c_str());
        default:
            throw SockEx("Error convering ip address %s", strerror(errno));
        }
    }
}


void TestSocket::bind(const std::string& sAddr, uint16_t port)
{
    struct sockaddr_in addr = addrFromString(sAddr, port);

    int one = 1;
    auto ret = ::setsockopt(m_fd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));
    if (ret < 0) {
        throw SockEx("Error setting sockopt socket %d, %s\n", errno, strerror(errno));
    }
    ret = ::bind(m_fd, (struct sockaddr*)(&addr), sizeof(addr));
    if (ret) {
        throw SockEx("Error binding socket %d, %s\n", errno, strerror(errno));
    }
}

void TestSocket::listen()
{
    auto ret = ::listen(m_fd, 5);
    if (ret) {
        throw SockEx("Error listening on socket %d, %s\n"
                , errno, strerror(errno));
    }
}

TestSocket TestSocket::accept()
{
    auto fd = ::accept(m_fd, nullptr, nullptr);
    if (fd < 0) {
        throw SockEx("Error accepting on socket %d, %s\n"
                , errno, strerror(errno));
    } else {
        return TestSocket(fd);
    }
}
    
void TestSocket::connect(std::string& sAddr, uint16_t port)
{
    struct sockaddr_in addr = addrFromString(sAddr, port);
    auto ret =::connect(m_fd, (struct sockaddr*)(&addr), sizeof(addr));
    if (ret) {
        throw SockEx("Error connecting socket %d, %s\n", errno, strerror(errno));
    }
}
    
std::string TestSocket::read(size_t len)
{
    std::string buf;
    buf.resize(len);
    char* p = &buf[0];
    while (len > 0) {

        auto ret = ::read(m_fd, p, len);
        if (ret < 0) {
            throw SockEx("Error reading from socket %d, %s\n", errno, strerror(errno));
        } else if (ret == 0) {
            //We have reached EOF
            break;
        } else {
            p += ret;
            len -= ret;
        }
    }
    buf.resize(p - buf.data());
    return buf;

}

void TestSocket::write(const std::string& data)
{
    const char* p = data.data();
    const char* pEnd = p + data.size();
    while (p < pEnd) {
        auto ret = ::write(m_fd, p, data.size() - (p - data.data()));
        if (ret < 0) {
            throw SockEx("Error writing to socket %d, %s\n", errno, strerror(errno));
        } else {
            p += ret;
        }
    }
}
