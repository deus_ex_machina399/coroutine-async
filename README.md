# Coroutine Demo Library

This is a simple demonstration on how one can use C++20 coroutines to build
asynchronous netowkring applications

This utilizes functionality that the ISO committee is planning to add into C++
in its 2020 release.

This project uses the experimental implmentation that is currently in clang.

The main demo program, runs a simple echo server.

Supports my blog at https://dwcomputersolutions.net

## Platform

Works only on linux since it uses `epoll`

## Build requirements

``` 
sudo apt-get install clang sudo apt-get install libc++ cmake
```

## Build

navigate to home directory 
```
./build.sh
```

## Usage

`./echoServer <port>`

This will start the echo server on port <port>. You can connect to it by using

```
telnet 127.0.0.1 20000
```

## Caveats 
This is very much a work in progress. Testing has been extremely
limited

## Documents
[N4775](http://open-std.org/JTC1/SC22/WG21/docs/papers/2018/n4775.pdf)




